// 云函数入口文件
const cloud = require("wx-server-sdk");

cloud.init();

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext();

  cloud.updateConfig({
    env: wxContext.ENV == "local" ? "test-go5gy" : wxContext.ENV
  });

  const db = cloud.database();

  const {
    mall_id, // 合集id
    book_name,
    book_auth,
    book_cover,
    book_source,
    read_chapter,
    book_page
  } = event;

  const res = await db
    .collection("book_shelf")
    .where({
      _openid: wxContext.OPENID,
      book_name:book_name,
      book_auth:book_auth
    })
    .get();

  if (res.data.length == 0) {
    // 新添加书架
    const add = await db.collection("book_shelf").add({
      data: {
        mall_id, // 合集id
        book_name,
        book_auth,
        book_cover,
        read_chapter,
        book_page, // 阅读章节的第几页  
        read_time: new Date().getTime(), // 加入时间，以供首页的排序
        book_source,// 出处
        _openid: wxContext.OPENID
      }
    });
    if (add.errMsg == "collection.add:ok") {
      return { errMsg: "ok", data: { _id: add._id } };
    } else {
      return { errMsg: add.errMsg };
    }
  }else {
    return {
      errMsg: "该书籍已在书架",
      data: res.data[0]
    };
  }
};
