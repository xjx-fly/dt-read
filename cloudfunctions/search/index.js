// 云函数入口文件
const cloud = require("wx-server-sdk");
const qs = require("qs");
const GBK = require("gbk.js");

cloud.init();

let db = cloud.database();
let _ = db.command;

/**
 * 搜索记录
 * @param {string} value
 * @param {string} openid
 */
async function serachLog(value, openid) {
  // 搜索历史记录
  db.collection("search_histories")
    .where({
      name: value,
      _openid: openid
    })
    .count()
    .then(res => {
      if (res.total > 0) {
        db.collection("search_histories")
          .where({
            name: value,
            _openid: openid
          })
          .update({
            data: {
              updated_at: new Date().getTime()
            }
          });
      } else {
        db.collection("search_histories").add({
          data: {
            name: value,
            _openid: openid,
            updated_at: new Date().getTime()
          }
        });
        // 最多保留3条搜索记录
        db.collection("search_histories")
          .where({
            _openid: openid
          })
          .count()
          .then(res => {
            if (res.total > 3) {
              db.collection("search_histories")
                .where({
                  _openid: openid
                })
                .orderBy("updated_at", "asc")
                .limit(1)
                .get()
                .then(res => {
                  db.collection("search_histories")
                    .doc(res.data[0]._id)
                    .remove();
                });
            }
          });
      }
    })
    .catch(console.error);

  // 搜索记录统计
  db.collection("search_count")
    .where({
      name: value
    })
    .count()
    .then(res => {
      if (res.total > 0) {
        db.collection("search_count")
          .where({
            name: value
          })
          .update({
            data: {
              count: _.inc(1)
            }
          });
      } else {
        db.collection("search_count").add({
          data: {
            name: value,
            count: 1
          }
        });
      }
    });
}

/**
 * 获得请求地址
 * @param {string} url
 * @param {string} type
 * @param {object} searchData
 * @param {string} value
 */
function getUrl(url, searchData, value) {
  const { key, extraData, type } = searchData;
  if (key) extraData[key] = value;
  let searchUrl = url + qs.stringify(extraData, { encode: false });
  if (type == "gbk") {
    searchUrl = GBK.URI.encodeURI(searchUrl);
  } else {
    searchUrl = encodeURI(searchUrl);
  }
  return searchUrl;
}

/**
 * 拼接post数据
 */
function getPostData(postData, value) {
  if (!postData) {
    return {}
  }
  const { key, extraData, type } = postData
  const data = {}
  if (key) {
    data[key] = type == "gbk" ? GBK.URI.encodeURI(value) : value
  }
  if (extraData) {
    for (const i in extraData) {
      data[i] = type == "gbk" ? GBK.URI.encodeURI(extraData[i]) : extraData[i]
    }
  }
  return data
}

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext();

  cloud.updateConfig({
    env: wxContext.ENV == "local" ? "test-go5gy" : wxContext.ENV
  });

  db = cloud.database();
  _ = db.command;

  const { pageIndex, value, type_code } = event;
  if (pageIndex == 1 && value != null && value != '' && value != 'undefined') {
    serachLog(value, wxContext.OPENID);
  }

  let resultDate = null;

  if(value != null || value != '' || value != 'undefined'){// 搜索内容不为空
    if(type_code == "0") {// 分类:全部合集
      resultDate = await db
      .collection("book_mall")
      .where({
        book_name: db.RegExp({  //正则表达式
          regexp: '.*' + value + '.*',
          options: 'i',
        }),
      })
      .get();
    }else {
      resultDate = await db
      .collection("book_mall")
      .where({
        book_name: db.RegExp({  //正则表达式
          regexp: '.*' + value + '.*',
          options: 'i',
        }),
        type_code: type_code
      })
      .get();
    }
  }else{
    if(type_code == "0") {
      resultDate = await db
      .collection("book_mall")
      .get();
    }else {
      resultDate = await db
      .collection("book_mall")
      .where({
        type_code: type_code
      })
      .get();
    }
  }

  let data = [];
  if (resultDate.errMsg == "collection.get:ok" && resultDate.data.length > 0) {
    data = resultDate.data;
  } else {
    return {
      errMsg: "查询无结果!"
    };
  }

  return { errMsg: "ok", data };
};
