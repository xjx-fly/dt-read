// 云函数入口文件
const cloud = require("wx-server-sdk");

cloud.init();

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext();

  cloud.updateConfig({
    env: wxContext.ENV
  });

  const db = cloud.database();

  const { mall_id } = event;
  const catalogData = await db
    .collection("book_catalog")
    .where({
      mall_id : mall_id
    })
    .get();
  if (catalogData.data) {
    return { errMsg: "ok", data: catalogData.data };
  } else {
    return { errMsg: "请求错误" };
  }
};
