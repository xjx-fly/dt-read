// 云函数入口文件
const cloud = require("wx-server-sdk");

cloud.init();

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext();

  cloud.updateConfig({
    env: wxContext.ENV == "local" ? "test-go5gy" : wxContext.ENV
  });

  const db = cloud.database();

  const { sources } = event;

  if (!sources || sources.length == 0) {
    return { errMsg: "没有源" };
  }

  const data = [];
  for (let source of sources) {
    const res = await cloud.callFunction({
      name: "bookDetail",
      data: {
        source_id: source.source_id,
        read_url: source.read_url
      }
    });
    if (res.result && res.result.errMsg == "ok") {
      data.push({ ...res.result.data, source_name: source.source_name });
    }
  }

  return {
    errMsg: "ok",
    data
  };
};
