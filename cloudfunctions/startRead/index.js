// 云函数入口文件
const cloud = require("wx-server-sdk");

cloud.init();

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext();

  cloud.updateConfig({
    env: wxContext.ENV == 'local' ? 'test-go5gy' : wxContext.ENV
  });

  const db = cloud.database();

  // const { source_id, url } = event;
  const { mall_id, read_chapter } = event;
  console.log(mall_id, read_chapter);

  // 获取书籍内容
  const contentList = await db
  .collection("book_context")
  .where({
    mall_id,
    read_chapter
  })
  .field({ content: true })
  .get();

  if (contentList.errMsg == "collection.get:ok" && contentList.data.length > 0) {
    const contentData = contentList.data[0];
    // console.log(res)
    if (!contentData) {
      return { errMsg: "请求错误" };
    }
    return { errMsg: "ok", data: contentData };
  } else {
    return {
      errMsg: contextList.errMsg,
      data
    };
  }

  // 获取源
  // const source = await db
  //   .collection("book_sources")
  //   .doc(source_id)
  //   .field({ content: true })
  //   .get();

  // // console.log(source, url)
  // if (source.data && source.data.content) {
  //   const res = await cloud.callFunction({
  //     name: "worm",
  //     data: {
  //       url: url,
  //       type: source.data.content.type,
  //       result: source.data.content.result
  //     }
  //   });
  //   // console.log(res)
  //   if (!res.result) {
  //     return { errMsg: "请求错误" };
  //   }
  //   return { errMsg: "ok", data: res.result };
  // } else {
  //   return {
  //     errMsg: source.errMsg,
  //     data
  //   };
  // }
};
