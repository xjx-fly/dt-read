// 云函数入口文件
const cloud = require("wx-server-sdk");

cloud.init();

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext();

  cloud.updateConfig({
    env: wxContext.ENV == 'local' ? 'test-go5gy' : wxContext.ENV
  });

  const db = cloud.database();

  const _id = event.mall_id;
  const bookMall = await db
    .collection("book_mall")
    .doc(_id)
    .get();

  if (bookMall.data) {
    return { errMsg: "ok", data: bookMall.data };
  } else {
    return { errMsg: "请求错误!" };
  }
};
